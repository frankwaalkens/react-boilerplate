const requireAll = r => {
    r.keys().forEach(r);
};

require('./style/app.scss');
requireAll(require.context('./_embedded-resources/svg-icons/', true, /\.svg$/));